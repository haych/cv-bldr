Template.references.personal = function(){

}

Template.references.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('references');
  $('.step-count h4').text('Step 4 - References');
  $('.step-count .fourth').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');

  $('#phone').mask('99999 999999');
}


Template.references.helpers({
  'references': function(){
      return References.find().fetch();
  }
});

Template.references.events({
  'change #noref' : function(e){
      var check = $("#noref");
      var name = $("#fullname");
      var job = $("#jtitle");
      var phone = $("#phone");
      var email = $("#email");
      var company = $("#company");
      if(check.is(':checked')){
        console.log("Is selected");
        name.prop('disabled', true);
        job.prop('disabled', true);
        phone.prop('disabled', true);
        email.prop('disabled', true);
        $("input:radio").prop("disabled", true);
        company.prop("disabled", true);
      } else{
        console.log("Isn't selected");
        name.prop('disabled', false);
        job.prop('disabled', false);
        phone.prop('disabled', false);
        email.prop('disabled', false);
        $("input:radio").prop("disabled", false);
        company.prop("disabled", false);
      }
  },
  'click #addref' : function(e){
    var check = $("#noref");
    var radio = $("input[name='type']:checked");
    var name = $("#fullname");
    var job = $("#jtitle");
    var phone = $("#phone");
    var email = $("#email");
    var company = $("#company");
    if(References.find().count() >= 1 && check.is(':checked')){
      console.log("please remove any current references to use this.")
      return;
    }
    if(check.is(':checked')){
      if(!Meteor.userId()){
        return;
      } else{
        References.insert({
          Owner: Meteor.userId(),
          Type: "",
          Name: "",
          Company: "",
          Job: "",
          Phone: "",
          Email: "",
          Ref: "References available upon request."
        })
      }
    } else{
      if(References.find({Ref: 'References available upon request.'}).count() >= 1){
        console.log("You can't add references whilst ");
        return;
      } else if(References.find().count() >= 2){
        return;
      }
      else if(!Meteor.userId() || !name || !job || !phone || !email){
        return;
      } else{
        References.insert({
          Owner: Meteor.userId(),
          Type: radio.val(),
          Name: name.val(),
          Company: company.val(),
          Job: job.val(),
          Phone: phone.val(),
          Email: email.val(),
          Ref: ""
        })
      }
    }
  },
  'click #continue' : function(e){
    if(References.find().count() == 0){
      if(confirm("You haven't entered any references, if you don't wish to enter references please choose references will be provided upon request. Do you want to continue?")){
        Router.go('themes');
      } else{
        console.log("stay");
      }
    } else{
      Router.go('themes');
    }
  },
  'click li' : function(e){
    console.log(this._id);
    if(confirm("Are you sure you want to delete this?")){
      References.remove({
        _id: this._id
      })
    } else{
      return;
    }
  }
});
