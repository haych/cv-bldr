Template.skills.personal = function(){

}

Template.skills.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('skills');
  $('.step-count h4').text('Step 4 - Skills');
  $('.step-count .fourth').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');
}

Template.skills.helpers({
    'skills': function(){
        return Skills.find().fetch();
    }
});

Template.skills.events({
  'click #addskill' : function(e){
    var radio = $("input[name='type']:checked");
    var title = $("#qtitle");

    if(!Meteor.userId() || !radio || !title || !radio.val()){
      return;
    } else{
      Skills.insert({
        Owner:Meteor.userId(),
        Type: radio.val(),
        Title: title.val()
      })
    }
  },
  'click #continue' : function(e){
    if(Skills.find().count() == 0){
      if(confirm("You haven't entered any qualifications or skills, do you want to continue?")){
        Router.go('references');
      } else{
        console.log("stay");
      }
    } else{
      Router.go('references');
    }
  },
  'click li' : function(e){
    console.log(this._id);
    if(confirm("Are you sure you want to delete this?")){
      Skills.remove({
        _id: this._id
      })
    } else{
      return;
    }
  }
});
