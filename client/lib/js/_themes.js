Template.themes.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('themes');
  $('.step-count h4').text('Step 5 - Themes');
  $('.step-count .fifth').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');

  var finder = Themes.find().fetch();
  var finderFirstName = finder[0].Theme;

  console.log(finderFirstName);
  document.themesOptions.theme.value=finderFirstName;

}


Template.themes.personal = function(){

}

Template.themes.helpers({
  'themes': function(){
      return Themes.find().fetch();
  }
});

Template.themes.events({
  'click #choose' : function(e){
    var radio = $("input[name='theme']:checked");
    var doc = Themes.findOne({Owner: Meteor.userId()});

    if(Themes.find().count() >= 1){
      Router.go("finish");
    }
    if(!Meteor.userId() ||  !radio.val()){
      console.log("nothing selected");
      return;
    } else if(Themes.findOne({Owner:Meteor.userId()})){
      Themes.update(doc["_id"], {$set: {
        Theme: radio.val()
      }});
    } else {
      if(Themes.find().count() >=1){
        console.log("theme chose");
        Router.go("finish");
        return;
      } else{
        Themes.insert({
          Owner: Meteor.userId(),
          Theme: radio.val()
        })
        Router.go('finish');
      }
    }

  }
});
