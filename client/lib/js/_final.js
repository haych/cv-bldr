Template.finish.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('finish');
  $('html, body').animate({ scrollTop: 0 }, 'fast');
}

Template.final.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('final');
  $('html, body').animate({ scrollTop: 0 }, 'fast');
}

Template.finish.helpers({
  'personal' : function(){
    return Personal.find().fetch();
  },
  'themes' : function(){
    return Themes.find().fetch();
  }
});


function exportPDF() {
  kendo.drawing.drawDOM("#print_inner", {
    paperSize:[790, 1000],
    multiPage: true
  }).then(function(group){
    kendo.drawing.pdf.saveAs(group, "CV.pdf");
  });
}

function share(){
  kendo.drawing.drawDOM("#print_inner", {
    paperSize:[790, 1000],
    multiPage: true
  }).then(function(group){
    kendo.drawing.pdf.toDataURL(group, function(dataURL){
      if(Shared.find().count() == 0){
        i = Math.random().toString(36).substring(7);
        if(Shared.find({URL: i}).count() == 1){
          share();
        }
        Shared.insert({
          Owner: Meteor.userId(),
          PDF: dataURL,
          URL: i
        });
        var url = Meteor.absoluteUrl() + "cv/" + i;
        $('#link').val(url);
        } else{
        console.log("you've already shared :(");
        var findId = Shared.find().fetch();
        var id = findId[0]._id;
        var url = findId[0].URL;
        Shared.update(id, {$set: {
          PDF: dataURL
        }});
        $('#link').val(Meteor.absoluteUrl() + "cv/" + Shared.find().fetch()[0].URL);
      }
    });
  });
}

Template.final.events({
  'click #download' : function(e){
    alert("Please select where you'd like your PDF to be saved");
    exportPDF();
  },
  'click #share' : function(e){
    share();
  },
  'click #link' : function (e){
    $("#link").focus(function() { $(this).select(); } );
  }
});

Template.final.helpers({
  'theme' : function(){
    return Themes.find().fetch();
  }
});

Template.themeone.helpers({
  'theme' : function(){
    return Themes.find().fetch();
  },
  'personal' : function(){
    return Personal.find().fetch();
  },
  'education' : function(){
    return Education.find().fetch();
  },
  'skills' : function(){
    return Skills.find().fetch();
  },
  'work' : function(){
    return Work.find().fetch();
  },
  'references' : function(){
    return References.find().fetch();
  }
});

Template.themetwo.helpers({
  'theme' : function(){
    return Themes.find().fetch();
  },
  'personal' : function(){
    return Personal.find().fetch();
  },
  'education' : function(){
    return Education.find().fetch();
  },
  'skills' : function(){
    return Skills.find().fetch();
  },
  'work' : function(){
    return Work.find().fetch();
  },
  'references' : function(){
    return References.find().fetch();
  }
});


Template.themethree.helpers({
  'theme' : function(){
    return Themes.find().fetch();
  },
  'personal' : function(){
    return Personal.find().fetch();
  },
  'education' : function(){
    return Education.find().fetch();
  },
  'skills' : function(){
    return Skills.find().fetch();
  },
  'work' : function(){
    return Work.find().fetch();
  },
  'references' : function(){
    return References.find().fetch();
  }
});

Template.registerHelper('equals', function (a, b) {
  return a === b;
});
