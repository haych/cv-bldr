Template.experience.personal = function(){

}

Template.experience.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('experience');
  $('.step-count h4').text('Step 3 - Experience');
  $('.step-count .third').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');

  $('#start').mask("99/99/9999");
  $('#end').mask("99/99/9999");

  var textarea = $('#jobdesc');
  var maxlength = textarea.attr("maxlength");
  var counta = $('#counta');
  counta.html(maxlength - textarea.val().length);
  textarea.keyup(function(){
    counta.html(maxlength - textarea.val().length);
  });
}


Template.experience.helpers({
    'work': function(){
        return Work.find().fetch();
    }
});

Template.experience.events({
  'click #addexp' : function(e){
    var title = $('#jobtitle');
    var company = $('#company');
    var start = $('#start');
    var end = $('#end');
    var description = $('#jobdesc');

    if (!Meteor.userId() || !title || !company || !start || !end || !description || title.val().length == 0 || company.val().length == 0 || start.val().length == 0 || end.val().length == 0 || description.val().length == 0){
      console.log("its not working");
      return;
    } else{
      Work.insert({
        Owner: Meteor.userId(),
        Title: title.val(),
        Company: company.val(),
        Start: start.val(),
        End: end.val(),
        Description: description.val()
      })
    }
  },
  'click #continue' : function(e){
     if(Work.find().count() == 0){
       if(confirm("You haven't entered any work experience, do you want to continue?")){
         Router.go('education');
       } else{
         console.log("stay");
       }
     } else{
       Router.go('education');
     }
  },
  'click li' : function(e){
    console.log(this._id);
    if(confirm("Are you sure you want to delete this?")){
      Work.remove({
        _id: this._id
      })
    } else{
      return;
    }
  },
  'click #jobdesc' : function(e) {
    $('#tips').text('Clearly outline what your role was whilst you were working at this company. Be sure to sure how you had a positive impact on the company. It is important to show this so employers can see how previous roles have prepared you for this new role.');
  }
});
