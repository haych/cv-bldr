Template.personalDetails.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('personal');
  $('.step-count h4').text('Step 2 - Details');
  $('.step-count .second').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');


  // Input Masks
  $('#phone').mask("99999 999999");
  $('#postcode').mask("aa99 9aa");

  var finder = Personal.find().fetch();
  if (finder.length == 0){
    console.log("no entry");
  } else{
    var finderFirstName = finder[0].FirstName;
    var finderLastName = finder[0].LastName;
    var finderPhone = finder[0].Phone;
    var finderPostcode = finder[0].Postcode;
    var finderHouseNo = finder[0].HouseNo;
    var finderAddress = finder[0].AddressLine;
    var finderAbout = finder[0].About;
    var finderInterest = finder[0].InterestHobbies;

    $('#fname').val(finderFirstName);
    $('#lname').val(finderLastName);
    $('#phone').val(finderPhone);
    $('#postcode').val(finderPostcode);
    $('#houseno').val(finderHouseNo);
    $('#address').val(finderAddress);
    $('#about').val(finderAbout);
    $('#interest').val(finderInterest);
  }
  var about = $('#about');
  var interest = $('#interest');
  var maxlength0 = about.attr("maxlength");
  var maxlength1 = interest.attr("maxlength");
  var counta = $('#counta');
  var counti = $('#counti');

  counta.html(maxlength0 - about.val().length);
  about.keyup(function(){
    counta.html(maxlength0 - about.val().length);
  });

  counti.html(maxlength1 - interest.val().length);
  interest.keyup(function(){
    counti.html(maxlength1 - interest.val().length);
  });
}

Template.personalDetails.personal = function(){

}


Template.personalDetails.helpers({
    'personal': function(){
        return Personal.find().fetch();
    }
});

Template.personalDetails.events({
  'click #continue' : function (e){
    var fname = $('#fname');
    var lname = $('#lname');
    var phone = $('#phone');
    var postcode = $('#postcode');
    var houseno = $('#houseno');
    var address = $('#address');
    var about = $('#about');
    var interest = $('#interest');
    var doc = Personal.findOne({Owner: Meteor.userId()});
    var g = JSON.stringify(doc);
    if (!Meteor.userId() || !fname || !lname || !phone || !postcode || !houseno || !address || !about || !interest){
      return;
    } else if (Personal.findOne({Owner: Meteor.userId()})) {
      Personal.update(doc["_id"], {$set: {
        FirstName:fname.val(),
        LastName:lname.val(),
        Phone:phone.val(),
        Email: Meteor.user().emails[0].address,
        Postcode:postcode.val(),
        HouseNo:houseno.val(),
        AddressLine:address.val(),
        About:about.val(),
        InterestHobbies:interest.val()
      }});
    } else{
      Personal.insert({
        Owner:Meteor.userId(),
        FirstName:fname.val(),
        LastName:lname.val(),
        Phone:phone.val(),
        Email:Meteor.user().emails[0].address,
        Postcode:postcode.val(),
        HouseNo:houseno.val(),
        AddressLine:address.val(),
        About:about.val(),
        InterestHobbies:interest.val()
      });
    }
    console.log(Personal.find({}, {fields: {'_id':1}}).fetch()[0]._id);
    Router.go('experience');
  },
  'click #about' : function(e){
    $('#tips').text("The about section will tell employers about your ambitions and your career goals. It will also include any important information you'd like employers to know as it will be at the top of the CV.");
  },
  'click #interest' : function(e){
    $('#tips').text('Your interests and hobbies are also a key factor, employers will want an insight into the kind of person you are. Write a short but concise paragraph on what you like to do in your free time and things that you have a keen interest in.');
  }

});
