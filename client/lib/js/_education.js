Template.education.personal = function(){

}

Template.education.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('education');
  $('.step-count h4').text('Step 3 - Education');
  $('.step-count .third').addClass('active');
  $('html, body').animate({ scrollTop: 0 }, 'fast');

  $('#start').mask("99/99/9999");
  $('#end').mask("99/99/9999");

  var textarea = $('#edudesc');
  var maxlength = textarea.attr("maxlength");
  var counta = $('#counta');
  counta.html(maxlength - textarea.val().length);
  textarea.keyup(function(){
    counta.html(maxlength - textarea.val().length);
  });
}


Template.education.helpers({
    'education': function(){
        return Education.find().fetch();
    }
});

Template.education.events({
  'click #addexp' : function(e){
    var course = $('#course');
    var school = $('#school');
    var start = $('#start');
    var end = $('#end');
    var description = $('#edudesc');
    var predicted = $('#predgrades');

    if(!Meteor.userId || !course || !school || !start || !end || !description || !predicted){
      return;
    } else{
      Education.insert({
        Owner: Meteor.userId(),
        Course: course.val(),
        School: school.val(),
        Start: start.val(),
        End: end.val(),
        Description: description.val(),
        Predicted: predicted.val()
      })
    }
  },
  'click #continue' : function(e){
     if(Education.find().count() == 0){
       if(confirm("You haven't entered any education, do you want to continue?")){
         Router.go('skills');
       } else{
         console.log("stay");
       }
     } else{
       Router.go('skills');
     }
  },
  'click li' : function(e){
    console.log(this._id);
    if(confirm("Are you sure you want to delete this?")){
      Education.remove({
        _id: this._id
      })
    } else{
      return;
    }
  },
  'click #edudesc' : function(e){
    $('#tips').text('When writing about your education mention key classes or modules that are relevant to the field you would like to pursue. Mention any modules or classes that may not be relevant but those you have an interest and those you have performed well in.');
  },
  'click #predgrades' : function(e){
    $('#tips').text('If you have finished the course enter the grades you received or if you are to finish soon then please enter the grades you are predicted. Predicted grades show employers what areas you excel in.');
  }
});
