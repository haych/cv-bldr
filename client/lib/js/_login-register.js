Template.register.events({
    'submit form': function(event){
        event.preventDefault();
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        console.log(password);
        Accounts.createUser({
            email: email,
            password: password,
        }, function(error){
          if(error){
            console.log(error.reason);
            $("#i").removeClass("invisible").addClass("visible").addClass("extra");
            document.getElementById('i').innerHTML = error.reason;
          } else{
            Router.go("personalDetails");
          }
        });
    }
});

Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(email, password, function(error){
          if(error){
              console.log(error.reason);
              $("#i").removeClass("invisible").addClass("visible").addClass("extra");
              document.getElementById('i').innerHTML = error.reason;
          } else {
            var finder = LastVisited.find().fetch();
            if(finder.length == 0){
              Router.go("personalDetails");
            } else{
              var currRouter = finder[0].Page;
              Router.go(currRouter);
            }
          }
        });
        }
});

Template.ForgotPassword.rendered = function(){
  $(document.body).removeClass();
  $(document.body).addClass('steps').addClass('forgot');
}

Template.ForgotPassword.events({
  'click .btn-submit': function(e, t) {
    email = $('#forgotPasswordEmail').val().toLowerCase();
    Accounts.forgotPassword({ email: email });
    alert('An email has been sent to you');
  }
});

if (Accounts._resetPasswordToken) {
  Session.set('resetPassword', Accounts._resetPasswordToken);
}

Template.ResetPassword.helpers({
 resetPassword: function(){
  return Session.get('resetPassword');
 }
});

Template.ResetPassword.events({
  'submit #resetPasswordForm': function(e, t) {
    e.preventDefault();

    var resetPasswordForm = $(e.currentTarget),
        password = resetPasswordForm.find('#resetPasswordPassword').val(),
        passwordConfirm = resetPasswordForm.find('#resetPasswordPasswordConfirm').val();

    if (isNotEmpty(password) && areValidPasswords(password, passwordConfirm)) {
      Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
        if (err) {
          console.log('We are sorry but something went wrong.');
        } else {
          console.log('Your password has been changed. Welcome back!');
          Session.set('resetPassword', null);
        }
      });
    }
    return false;
  }
});
