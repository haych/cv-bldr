import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';


// import './main.html';
// import './templates/login-register.html';

// this is equivalent to the standard node require:
Retina({retinajs: true, attribute : 'data-retina'});

Router.route('/',{
  name: 'home',
  template: 'home'
});
Router.route('/register');
Router.route('/login');
Router.route('/ResetPassword');
Router.route('/ForgotPassword');
Router.route('/personalDetails');
Router.route('/experience');
Router.route('/education');
Router.route('/skills');
Router.route('/references');
Router.route('/themes');

Router.route('/finish');
Router.route('/final', {
    name: 'final',
    layoutTemplate: '',
    data: function(){
        console.log("This is a list page.");
    }
});

Router.route('/cv/:_id', function () {
  this.render('cv', {
    data: function () {
      return Shared.findOne({URL: this.params._id});
    }
  });
});
Router.configure({
  layoutTemplate: 'main'
});

Meteor.subscribe('personal');

Meteor.subscribe('lastvisited');

Meteor.subscribe('work');

Meteor.subscribe('education');

Meteor.subscribe('skills');

Meteor.subscribe('references');

Meteor.subscribe('themes');

Meteor.subscribe('shared');


Template.nav.events({

});

Template.home.events({
  'click .arw-down' : function(e) {
    $('html, body').animate({
        scrollTop: $("#main-three").offset().top
    }, 1000);
  }
});

Template.body.events({
  'click #back' : function(e){
    history.back();
  },
  'click .logout': function(event){
    if(confirm("Are you sure you want to logout?")){
      event.preventDefault();
      var currRouter = Router.current().route.getName();
      var doc = LastVisited.findOne({Owner: Meteor.userId()});
      console.log(currRouter);
      if(LastVisited.find().count() == 0){
        LastVisited.insert({
          Owner: Meteor.userId(),
          Page: currRouter
        });
      } else{
        LastVisited.update(doc["_id"], {$set: {
          Page: currRouter
        }});
      }
      Meteor.logout();
      Router.go('login');
    } else{
      console.log("banter");
    }
  }
});
