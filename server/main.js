import { Meteor } from 'meteor/meteor';
Meteor.startup(function() {
  
});

Meteor.publish('personal', function(){
  return Personal.find({Owner:this.userId});
})

Meteor.publish('lastvisited', function(){
  return LastVisited.find({Owner:this.userId});
})

Meteor.publish('shared', function(){
  return Shared.find();
})

Meteor.publish('work', function(){
  return Work.find({Owner:this.userId});
})

Meteor.publish('education', function(){
  return Education.find({Owner:this.userId});
})

Meteor.publish('skills', function(){
  return Skills.find({Owner:this.userId});
})

Meteor.publish('references', function(){
  return References.find({Owner:this.userId});
})

Meteor.publish('themes', function(){
  return Themes.find({Owner:this.userId});
})

Personal.allow({
  insert: function(userId, fields){
    return(userId); //Makes sure user is logged in
  },
  update: function(userId, fields){
    return(userId);
  }
})

LastVisited.allow({
  insert: function(userId, fields){
    return(userId); //Makes sure user is logged in
  },
  update: function(userId, fields){
    return(userId);
  }
})

Work.allow({
  insert: function(userId, fields){
    return(userId);
  },
  remove: function(userId, fields){
    return(userId);
  }
})

Education.allow({
  insert: function(userId, fields){
    return(userId);
  },
  remove: function(userId, fields){
    return(userId);
  }
})

Skills.allow({
  insert: function(userId, fields){
    return(userId);
  },
  remove: function(userId, fields){
    return(userId);
  }
})

References.allow({
  insert: function(userId, fields){
    return(userId);
  },
  remove: function(userId, fields){
    return(userId);
  }
})

Shared.allow({
  insert: function(userId, fields){
    return(userId);
  },
  update: function(userId, fields){
    return(userId);
  }
})

Themes.allow({
  insert: function(userId, fields){
    return(userId);
  },
  update: function(userId, fields){
    return(userId);
  }
})
