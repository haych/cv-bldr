Personal = new Mongo.Collection('personal');
LastVisited = new Mongo.Collection('lastvisited');
Work = new Mongo.Collection('work');
Education = new Mongo.Collection('education');
Skills = new Mongo.Collection('skills');
References = new Mongo.Collection('references');
Themes = new Mongo.Collection('themes');
Shared = new Mongo.Collection('shared');
